//
//  AlertMessageGenerator.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 30/09/2020.
//

import Foundation

struct AlertMessageGenerator {
    
    static func createAlertMessage(locationName: String) -> String {
        return Strings.forecastForLocationMessage + "\"\(locationName)\"" + Strings.exclamationMark
    }
}
