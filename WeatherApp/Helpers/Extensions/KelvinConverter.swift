//
//  KelvinConverter.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 25/09/2020.
//

import Foundation

extension Double {
    func convertKelvinToCelsius() -> Double {
        let absoluteZero = 273.15
        return (self - absoluteZero).rounded()
    }
}
