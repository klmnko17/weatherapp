//
//  GetCityName.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 01/10/2020.
//

import Foundation

extension String {
    
    var cityName: String {
        String(self.split(separator: ",")[0])
    }
}
