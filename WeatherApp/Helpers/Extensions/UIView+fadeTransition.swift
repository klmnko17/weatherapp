//
//  UIView+fadeTransition.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 25/09/2020.
//

import UIKit

extension UIView {
    
    // MARK: Solution for UIView fade transition found on StackOverflow
    func fadeTransition(_ duration: CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(
            name:CAMediaTimingFunctionName.easeInEaseOut
        )
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(
            animation,
            forKey: CATransitionType.fade.rawValue
        )
    }
}
