//
//  DropLastCharacters.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 29/09/2020.
//

import Foundation

extension String {
    func dropLastCharacters(count: Int) -> String {
        String(self.dropLast(3))
    }
}
