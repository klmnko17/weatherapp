//
//  WeatherIcon.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 29/09/2020.
//

import UIKit

enum WeatherCardImageKeys: String {
    case atmosphere
    case cloud
    case lightRain = "light-rain"
    case rain
    case snow
    case storm
    case sun

    static func getWeatherImage(id: Int) -> UIImage? {
        switch id {
        case 200...232:
            return UIImage(named: self.storm.rawValue)
        case 300...321:
            return UIImage(named: self.lightRain.rawValue)
        case 500...532:
            return UIImage(named: self.rain.rawValue)
        case 600...622:
            return UIImage(named: self.snow.rawValue)
        case 701...781:
            return UIImage(named: self.atmosphere.rawValue)
        case 800:
            return UIImage(named: self.sun.rawValue)
        case 801...804:
            return UIImage(named: self.cloud.rawValue)
        default:
            return nil
        }
    }
}
