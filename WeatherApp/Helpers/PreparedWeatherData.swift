//
//  PreparedWeatherData.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 01/10/2020.
//

import UIKit

struct PreparedWeatherData {
    var cityName: String?
    var celsiusDegrees: String?
    var weatherImage: UIImage?
    var snapshotImage: UIImage?
    var weatherDescription: String?
    var timestamp: String?
    
    init(
        cityName: String? = nil,
        celsiusDegrees: String? = nil,
        weatherImage: UIImage? = nil,
        snapshotImage: UIImage? = nil,
        weatherDescription: String? = nil,
        timestamp: String? = nil
    ) {
        self.cityName = cityName
        self.celsiusDegrees = celsiusDegrees
        self.weatherImage = weatherImage
        self.snapshotImage = snapshotImage
        self.weatherDescription = weatherDescription
        self.timestamp = timestamp
    }
}
