//
//  EnableSearchBarButton.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 30/09/2020.
//

import UIKit

struct EnableSearchBarButton {
    
    private static let cancelButtonKey = "cancelButton"

    // MARK: Solution for SearchBar cancel button enabling found here:
    // https://stackoverflow.com/questions/24072069
    static func enableCancelButton(searchBar: UISearchBar) {
        DispatchQueue.main.async {
            if let cancelButton = searchBar.value(
                forKey: self.cancelButtonKey
            ) as? UIButton {
                cancelButton.isEnabled = true
            }
        }
    }
}
