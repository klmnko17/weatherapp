//
//  TimestampGenerator.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 29/09/2020.
//

import Foundation

class TimestampGenerator {
    
    private static let formatter = DateFormatter()
    
    static func generateCurrentTimeStamp() -> String {
        self.formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return self.formatter.string(from: Date())
    }
}
