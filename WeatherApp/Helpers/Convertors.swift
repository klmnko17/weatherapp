//
//  Convertors.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 29/09/2020.
//

import MapKit

struct Convertors {
    
    private static let celsiusSymbol = "°"
    
    static func createTemperatureString(from kelvin: Double) -> String {
        String(format: "%g", kelvin.convertKelvinToCelsius()) + self.celsiusSymbol
    }
    
    static func createCoordinatesParameterString(
        from coordinatesParameter: CLLocationDegrees
    ) -> String {
        String(format: "%f", coordinatesParameter)
    }
}
