//
//  SceneDelegate.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 24/09/2020.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
        
        // This part of the code should be here for the proper working of spinners from SVProgressHUD library.
        AppDelegate.standard.window = window
    }
}
