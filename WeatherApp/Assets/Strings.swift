//
//  Strings.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 25/09/2020.
//

import Foundation

struct Strings {
    
    static let exclamationMark = "?"
    static let warningTitle = "Warning⚠️"
    // MARK: RootView strings
    static let turnOnLocationServicesMessage = "Turn on Location services in Settings"
    static let authorizeLocationAccessMessage = "Allow location access for WeatherApp in Settings"
    static let unknownWeather = "Unknown"
    static let impossibleToGetData = "Impossible to get data"
    static let unknownCelsiusDegrees = "--°"
    static let noLocationName = "No location name"
    // MARK: Alert strings
    static let ok = "OK"
    static let cancel = "Cancel"
    static let yes = "Yes"
    static let forecastForLocationTitle = "Forecast Location"
    static let forecastForLocationMessage = "Do you want to find and show the weather forecast for the following location: "
    static let forecastForMapLocationMessage = "Do you want to find and show the weather forecast for chosen on map location?"
    // MARK: LocationSearchView strings
    static let searchBarPlaceholderText = "Search"
    // MARK: Error strings
    static let errorTitle = "Error"
    static let genericError = "Oops! Something went wrong!"
    static let invalidURL = "Invalid URL"
    static let realmSaveError = "Error during saving Realm object"
    static let realmCreateInstanceError = "Error during creating Realm instance"
    static let realmDeleteError = "Error during deleting Realm object"
    static let realmDataWasNotPrepared = "Error during preparing Realm data"
    static let noInternetConnection = "The Internet connection appears to be offline."
    static let unauthorized = "The request has not been applied because it lacks valid authentication credentials for the target resource."
    static let notFound = "You specify wrong city name or weather forecast for that city wasn't found"
    static let badRequest = "The server cannot or will not process the request due to something that is perceived to be a client error"
    static let tooManyRequests = "It was sent too many requests in a given amount of time(60 API calls per minute for a free account)"
    static let impossibleToFetchAddress = "It was impossible to fetch location address please check internet connection and try again"
}
