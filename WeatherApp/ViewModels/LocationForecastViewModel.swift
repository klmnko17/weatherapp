//
//  LocationForecastViewModel.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 28/09/2020.
//

import Foundation

protocol LocationForecastViewModelDelegate: AnyObject {
    func refreshData()
    func showLoadingSpinner()
    func hideLoadingSpinner()
    func showInfoAlert(
        title: String,
        message: String,
        completion: (() -> Void)?
    )
    func pushRootScreen(identifier: String)
}

class LocationForecastViewModel {
    
    private weak var delegate: LocationForecastViewModelDelegate?
    private let apiClient = ApiClient.shared
    private(set) var fetchedForecastData: ForecastWeather? {
        didSet {
            self.delegate?.refreshData()
        }
    }
    let cellIdentifier = "LocationForecastCell"
    let navigationBarTitle = "Location Forecast"
    private let rootScreenIdentifier = "Main"
    var cityName: String?
    
    init(delegate: LocationForecastViewModelDelegate?) {
        self.delegate = delegate
    }
    
    func getSearchedLocationCityName() {
        self.delegate?.showLoadingSpinner()
        guard let cityName = self.cityName,
              let encoodedCityName = cityName.addingPercentEncoding(
                withAllowedCharacters: .urlQueryAllowed
              )
        else {
            self.delegate?.showInfoAlert(
                title: Strings.errorTitle,
                message: WeatherAppError.genericError.getErrorString()
            ) {
                self.delegate?.pushRootScreen(identifier: self.rootScreenIdentifier)
            }
            return
        }
        self.apiClient.fetchWeatherForecast(for: encoodedCityName) { (data, error) in
            guard let data = data else {
                let error = error ?? WeatherAppError.genericError
                self.delegate?.hideLoadingSpinner()
                self.delegate?.showInfoAlert(
                    title: Strings.errorTitle,
                    message: error.getErrorString()
                ) {
                    self.delegate?.pushRootScreen(identifier: self.rootScreenIdentifier)
                }
                return
            }
            self.fetchedForecastData = data
            self.delegate?.hideLoadingSpinner()
            self.delegate?.refreshData()
        }
    }
    
    func getNumberOfRowsInSection() -> Int {
        return self.fetchedForecastData?.list.count ?? 0
    }
    
    func prepareCellData(indexPath: IndexPath) -> PreparedWeatherData? {
        guard let data = self.fetchedForecastData,
              let weatherId = data.list[indexPath.row].weather.first?.id
        else { return nil }
        let preparedData = PreparedWeatherData(
            cityName: data.city.name,
            celsiusDegrees: Convertors.createTemperatureString(
                from: data.list[indexPath.row].temperature.kelvin
            ),
            weatherImage: WeatherCardImageKeys.getWeatherImage(
                id: weatherId
            ),
            timestamp: data.list[indexPath.row].dateString.dropLastCharacters(
                count: 3
            )
        )
        return preparedData
    }
}
