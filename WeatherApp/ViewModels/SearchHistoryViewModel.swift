//
//  SearchHistoryViewModel.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 29/09/2020.
//

import Foundation
import RealmSwift

protocol SearchHistoryViewModelDelegate: AnyObject {
    func showChangeWeatherForLocationAlert(
        title: String,
        message: String,
        completion: @escaping () -> ()
    )
    func dismissView()
    func passDataToRootViewController(cityName: String)
    func refreshData()
}

class SearchHistoryViewModel {
    
    private weak var delegate: SearchHistoryViewModelDelegate?
    private let persistentStorageClient = PersistentStorageClient.shared
    private(set) var searchedLocations: Results<SearchedLocation>!
    private(set) var filteredSearchedLocations = [SearchedLocation]() {
        didSet {
            self.delegate?.refreshData()
        }
    }
    lazy private var numberOfRowsInSection = self.searchedLocations.count
    private var filtered = false
    let cellIdentifier = "SearchHistoryCell"
    
    func loadAllSearchedLocations() {
        self.searchedLocations = self.persistentStorageClient.loadSortedObjects()
    }
    
    init(delegate: SearchHistoryViewModelDelegate?) {
        self.delegate = delegate
    }

    func chooseLocationForWeatherForecast(at indexPath: IndexPath) {
        var locationName = self.searchedLocations[indexPath.row].locationName
        if !self.filteredSearchedLocations.isEmpty {
            locationName = self.filteredSearchedLocations[indexPath.row].locationName
        }
        self.delegate?.showChangeWeatherForLocationAlert(
            title: Strings.forecastForLocationTitle,
            message: AlertMessageGenerator.createAlertMessage(
                locationName: locationName
            )
        ) {
            self.delegate?.passDataToRootViewController(cityName: locationName)
            self.delegate?.dismissView()
        }
    }
    
    func filterSearchedLocations(searchText: String) {
        self.filtered = false
        if !searchText.isEmpty {
            self.filtered = true
        }
        self.filteredSearchedLocations = self.searchedLocations.filter {
            $0.locationName.lowercased().contains(searchText)
        }
    }
    
    func prepareCellData(indexPath: IndexPath) -> SearchedLocation {
        var cellData = self.searchedLocations[indexPath.row]
        if !self.filteredSearchedLocations.isEmpty {
            cellData = self.filteredSearchedLocations[indexPath.row]
        }
        return cellData
    }
    
    func prepareCellData(indexPath: IndexPath) -> PreparedWeatherData {
        var cellData = self.searchedLocations[indexPath.row]
        if !self.filteredSearchedLocations.isEmpty {
            cellData = self.filteredSearchedLocations[indexPath.row]
        }
        let preparedData = PreparedWeatherData(
            cityName: cellData.locationName,
            timestamp: cellData.timestamp.dropLastCharacters(count: 3)
        )
        return preparedData
    }
    
    func getNumberOfRowsInSection() -> Int {
        var numberOfRows = self.numberOfRowsInSection
        if !self.filteredSearchedLocations.isEmpty || self.filtered  {
            numberOfRows = self.filteredSearchedLocations.count
        }
        return numberOfRows
    }
}
