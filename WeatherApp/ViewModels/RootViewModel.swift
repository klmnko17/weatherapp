//
//  RootViewModel.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 25/09/2020.
//

import Foundation
import CoreLocation
import MapKit

protocol RootViewModelDelegate: AnyObject {
    func showInfoAlert(
        title: String,
        message: String,
        completion: (() -> Void)?
    )
    func setMapViewRegion(region: MKCoordinateRegion)
    func preparePointForMapAnnotation(
        region: MKCoordinateRegion,
        snapshot: MKMapSnapshotter.Snapshot,
        snapshotImage: UIImage,
        finalImageRect: CGRect,
        mapAnnotation: MKPinAnnotationView,
        mapAnnotationImage: UIImage
    )
    func showChangeWeatherForLocationAlert(
        title: String,
        message: String,
        completion: @escaping () -> ()
    )
    func updateWeatherCard(preparedData: PreparedWeatherData)
    func startAnimatingActivityIndicator()
    func stopAnimatingActivityIndicator()
}

class RootViewModel {
    
    private weak var delegate: RootViewModelDelegate?
    private weak var locationManagerDelegate: CLLocationManagerDelegate?
    private let locationClient = LocationClient.shared
    private let apiClient = ApiClient.shared
    private let persistentStorageClient = PersistentStorageClient.shared
    private let regionInMeters: Double = 30000
    private let snapshotCoordinatesMeters: Double = 10000
    private let snapshotSize = CGSize(width: 150, height: 150)
    private let unknownWeatherImageId = 200
    let searchScreenIdentifier = "LocationSearchScreen"
    let forecastScreenIdentifier = "LocationForecastScreen"
    let historyScreenIdentifier = "SearchHistoryScreen"
    private var weatherDataLatitude: Double = 0
    private var weatherDataLongitude: Double = 0
    var cityName: String?
    private var preparedData = PreparedWeatherData()

    init(
        delegate: RootViewModelDelegate?,
        locationManagerDelegate: CLLocationManagerDelegate?
    ) {
        self.delegate = delegate
        self.locationManagerDelegate = locationManagerDelegate
    }
    
    func checkIfLocationServicesAreEnabled() {
        let locationServicesAreEnabled = CLLocationManager.locationServicesEnabled()
        if locationServicesAreEnabled {
            self.setupLocationManager()
            self.checkLocationAuthorizationStatus()
        } else {
            self.delegate?.showInfoAlert(
                title: Strings.warningTitle,
                message: Strings.turnOnLocationServicesMessage,
                completion: nil
            )
        }
    }
    
    private func setupLocationManager() {
        self.locationClient.locationManager.delegate = self.locationManagerDelegate
        self.locationClient.locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func checkLocationAuthorizationStatus() {
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            self.zoomViewOnUserLocation()
            self.locationClient.locationManager.startUpdatingLocation()
            self.getWeatherDataForCurrentLocation()
        case .denied:
            self.prepareDataAndUpdateWeatherCardWhenAccessDenied()
            self.delegate?.showInfoAlert(
                title: Strings.warningTitle,
                message: Strings.authorizeLocationAccessMessage,
                completion: nil
            )
        case .notDetermined:
            self.locationClient.locationManager.requestAlwaysAuthorization()
        case .restricted:
            break
        // MARK: It's Apple suggession to have @unknown default case with fatalError() here for possible updates in the future
        // Switch covers known cases, but 'CLAuthorizationStatus' may have additional unknown values, possibly added in future versions
        @unknown default:
            fatalError()
        }
    }
    
    private func zoomViewOnUserLocation() {
        guard let location = self.locationClient.currentLocationCoordinate else {
            self.delegate?.showInfoAlert(
                title: Strings.errorTitle,
                message: WeatherAppError.genericError.getErrorString(),
                completion: nil
            )
            return
        }
        let region = MKCoordinateRegion(
            center: location,
            latitudinalMeters: self.regionInMeters,
            longitudinalMeters: self.regionInMeters
        )
        self.delegate?.setMapViewRegion(region: region)
    }
    
    // MARK: - Solution for mapSnapshot found here:
    // https://stackoverflow.com/questions/18776288
    private func prepareMapSnapshotterOptions(
        region: MKCoordinateRegion
    ) -> MKMapSnapshotter.Options {
        let options = MKMapSnapshotter.Options()
        options.region = region
        options.scale = UIScreen.main.scale
        options.size = self.snapshotSize
        return options
    }
    
    private func createMapSnapshotWithPin(
        latitude: CLLocationDegrees,
        longitude: CLLocationDegrees,
        completion: @escaping (UIImage?) -> ()
    ) {
        let region = MKCoordinateRegion(
            center: CLLocationCoordinate2D(
                latitude: latitude,
                longitude: longitude
            ),
            latitudinalMeters: self.snapshotCoordinatesMeters,
            longitudinalMeters: self.snapshotCoordinatesMeters
        )
        let mapSnapshotter = MKMapSnapshotter(
            options: self.prepareMapSnapshotterOptions(region: region)
        )
        mapSnapshotter.start { snapshot, error in
            guard let snapshot = snapshot else {
                return
            }
            let snapshotImage = self.createAndConfigureMapSnapshotImage(
                snapshot: snapshot,
                region: region
            )
            completion(snapshotImage)
        }
    }
    
    private func createAndConfigureMapSnapshotImage(
        snapshot: MKMapSnapshotter.Snapshot,
        region: MKCoordinateRegion
    ) ->  UIImage? {
        let snapshotImage = snapshot.image
        let finalImageRect = CGRect(
            x: 0,
            y: 0,
            width: snapshotImage.size.width,
            height: snapshotImage.size.height
        )
        let mapAnnotation = MKPinAnnotationView(
            annotation: nil,
            reuseIdentifier: nil
        )
        guard let mapAnnotationImage = mapAnnotation.image else { return nil }
        UIGraphicsBeginImageContextWithOptions(
            snapshotImage.size,
            true,
            snapshotImage.scale
        )
        snapshotImage.draw(at: CGPoint(x: 0, y: 0))
        self.delegate?.preparePointForMapAnnotation(
            region: region,
            snapshot: snapshot,
            snapshotImage: snapshotImage,
            finalImageRect: finalImageRect,
            mapAnnotation: mapAnnotation,
            mapAnnotationImage: mapAnnotationImage
        )
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return finalImage
    }
    
    private func getWeatherData(for coordinates: CLLocationCoordinate2D) {
        self.apiClient.fetchCurrentWeatherWithCoordinates(
            longitude: Convertors.createCoordinatesParameterString(
                from: coordinates.longitude
            ),
            latitude: Convertors.createCoordinatesParameterString(
                from: coordinates.latitude
            )
        ) { (data, error) in
            guard let data = data else {
                let error = error ?? WeatherAppError.genericError
                self.delegate?.showInfoAlert(
                    title: Strings.errorTitle,
                    message: error.getErrorString(),
                    completion: nil
                )
                self.delegate?.stopAnimatingActivityIndicator()
                return
            }
            var cityName = data.cityName
            if data.cityName.isEmpty {
                cityName = Strings.noLocationName
            }
            self.persistentStorageClient.saveObject(
                with: cityName
            )
            self.weatherDataLatitude = data.coordinates.latitude
            self.weatherDataLongitude = data.coordinates.longitude
            self.createMapSnapshotWithPin(
                latitude: self.weatherDataLatitude,
                longitude: self.weatherDataLongitude
            ) { snapshotImage in
                self.prepareDataAndUpdateWeatherCard(
                    data: data,
                    snapshotImage: snapshotImage
                )
            }
        }
    }
    
    func saveMapLocationCoordinates(coordinates: CLLocationCoordinate2D) {
        self.delegate?.showChangeWeatherForLocationAlert(
            title: Strings.forecastForLocationTitle,
            message: Strings.forecastForMapLocationMessage
        ) {
            self.getWeatherData(for: coordinates)
        }
    }
    
    func getWeatherDataForCurrentLocation() {
        self.delegate?.startAnimatingActivityIndicator()
        guard let locationLatitude = self.locationClient.currentLocationCoordinate?.latitude,
              let locationLongitude = self.locationClient.currentLocationCoordinate?.longitude
        else { return }
        self.getLocationAddress(
            latitude: locationLatitude,
            longitude: locationLongitude
        ) { cityName in
            self.fetchCurrentWeather(
                for: cityName,
                locationIsCurrentLocation: true
            )
        }
    }
    
    private func getLocationAddress(
        latitude: CLLocationDegrees,
        longitude: CLLocationDegrees,
        completion: @escaping (String) -> ()
    ) {
        self.locationClient.getLocationAddress(
            for: latitude,
            for: longitude
        ) { locationAddress in
            guard let locationAddress = locationAddress else {
                self.delegate?.showInfoAlert(
                    title: Strings.errorTitle,
                    message: WeatherAppError.impossibleToFetchAddress.getErrorString(),
                    completion: nil
                )
                return
            }
            guard let encoodedCityName = locationAddress.addingPercentEncoding(
                withAllowedCharacters: .urlQueryAllowed
            ) else {
                self.delegate?.showInfoAlert(
                    title: Strings.errorTitle,
                    message: WeatherAppError.genericError.getErrorString(),
                    completion: nil
                )
                return
            }
            completion(encoodedCityName)
        }
    }
    
    func getWeatherData(for cityName: String) {
        self.delegate?.startAnimatingActivityIndicator()
        guard let encoodedCityName = cityName.addingPercentEncoding(
            withAllowedCharacters: .urlQueryAllowed
        ) else {
            self.delegate?.showInfoAlert(
                title: Strings.errorTitle,
                message: WeatherAppError.genericError.getErrorString(),
                completion: nil
            )
            self.delegate?.stopAnimatingActivityIndicator()
            return
        }
        self.fetchCurrentWeather(
            for: encoodedCityName,
            locationIsCurrentLocation: false
        )
    }
    
    private func fetchCurrentWeather(
        for city: String,
        locationIsCurrentLocation: Bool
    ) {
        self.apiClient.fetchCurrentWeather(for: city) { (data, error) in
            guard let data = data else {
                let error = error ?? WeatherAppError.genericError
                self.delegate?.showInfoAlert(
                    title: Strings.errorTitle,
                    message: error.getErrorString(),
                    completion: nil
                )
                self.delegate?.stopAnimatingActivityIndicator()
                return
            }
            self.weatherDataLatitude = data.coordinates.latitude
            self.weatherDataLongitude = data.coordinates.longitude
            self.createMapSnapshotWithPin(
                latitude: self.weatherDataLatitude,
                longitude: self.weatherDataLongitude
            ) { snapshotImage in
                self.prepareDataAndUpdateWeatherCard(
                    data: data,
                    snapshotImage: snapshotImage
                )
            }
        }
    }
    
    private func prepareDataAndUpdateWeatherCard(
        data: CurrentWeather,
        snapshotImage: UIImage?
    ) {
        self.prepareData(data: data, snapshotImage: snapshotImage)
        self.delegate?.updateWeatherCard(preparedData: self.preparedData)
    }
    
    private func prepareData(
        data: CurrentWeather,
        snapshotImage: UIImage?
    ) {
        guard let weather = data.weather.first,
              let snapshotImage = snapshotImage
        else {
            self.prepareDataForAccessDeniedCase()
            return
        }
        self.cityName = data.cityName
        self.preparedData.cityName = data.cityName
        if data.cityName.isEmpty {
            self.cityName = Strings.noLocationName
            self.preparedData.cityName = Strings.noLocationName
        }
        self.preparedData.celsiusDegrees = Convertors.createTemperatureString(
            from: data.temperature.kelvin
        )
        self.preparedData.weatherImage = WeatherCardImageKeys.getWeatherImage(
            id: weather.id
        )
        self.preparedData.snapshotImage = snapshotImage
        self.preparedData.weatherDescription = weather.weatherDescription.capitalized
    }
    
    private func prepareDataAndUpdateWeatherCardWhenAccessDenied() {
        self.prepareDataForAccessDeniedCase()
        self.delegate?.updateWeatherCard(preparedData: self.preparedData)
    }
    
    private func prepareDataForAccessDeniedCase() {
        self.preparedData.cityName = Strings.impossibleToGetData
        self.preparedData.celsiusDegrees = Strings.unknownCelsiusDegrees
        self.preparedData.weatherImage = WeatherCardImageKeys.getWeatherImage(
            id: self.unknownWeatherImageId
        )
        self.preparedData.weatherDescription = Strings.unknownWeather
    }
}
