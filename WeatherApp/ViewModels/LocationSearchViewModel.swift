//
//  LocationSearchViewModel.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 26/09/2020.
//

import Foundation
import MapKit

protocol LocationSearchViewModelDelegate: AnyObject {
    func refreshData()
    func showChangeWeatherForLocationAlert(
        title: String,
        message: String,
        completion: @escaping () -> ()
    )
    func dismissView()
    func passDataToRootViewController(cityName: String)
}

class LocationSearchViewModel {
    
    private weak var delegate: LocationSearchViewModelDelegate?
    private let persistentStorageClient = PersistentStorageClient.shared
    let cellIdentifier = "SearchResultCell"
    var searchResults = [String]() {
        didSet {
            self.delegate?.refreshData()
        }
    }

    init(delegate: LocationSearchViewModelDelegate?) {
        self.delegate = delegate
    }

    func chooseLocationForWeatherForecast(at indexPath: IndexPath) {
        self.delegate?.showChangeWeatherForLocationAlert(
            title: Strings.forecastForLocationTitle,
            message: AlertMessageGenerator.createAlertMessage(
                locationName: self.searchResults[indexPath.row]
            )
        ) {
            self.persistentStorageClient.saveObject(
                with: self.searchResults[indexPath.row].cityName
            )
            self.delegate?.passDataToRootViewController(
                cityName: self.searchResults[indexPath.row].cityName
            )
            self.delegate?.dismissView()
        }
    }
    
    func processTextChangeInSearchBar(
        searchText: String,
        localSearchCompleter: MKLocalSearchCompleter
    ) {
        if searchText.isEmpty {
            self.searchResults = []
        }
        localSearchCompleter.queryFragment = searchText
    }
}
