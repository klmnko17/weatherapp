//
//  SearchHistoryCell.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 29/09/2020.
//

import UIKit

class SearchHistoryCell: UITableViewCell {

    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var date: UILabel!
    
    override func prepareForReuse() {
        self.cityName.text = nil
        self.date.text = nil
    }
    
    func configure(data: PreparedWeatherData) {
        self.cityName.text = data.cityName
        self.date.text = data.timestamp
    }
}
