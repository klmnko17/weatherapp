//
//  SearchResultCell.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 28/09/2020.
//

import UIKit

class SearchResultCell: UITableViewCell {

    @IBOutlet weak var locationName: UILabel!
    
    override func prepareForReuse() {
        self.locationName.text = nil
    }
    
    func configureCell(locationName: String) {
        self.locationName.text = locationName
    }
}
