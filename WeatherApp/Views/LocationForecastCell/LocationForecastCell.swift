//
//  LocationForecastCell.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 28/09/2020.
//

import UIKit

class LocationForecastCell: UITableViewCell {
    
    @IBOutlet weak var forecastImage: UIImageView!
    @IBOutlet weak var forecastCity: UILabel!
    @IBOutlet weak var forecastDate: UILabel!
    @IBOutlet weak var forecastTemperature: UILabel!
    
    override func prepareForReuse() {
        self.forecastImage.image = nil
        self.forecastCity.text = nil
        self.forecastDate.text = nil
        self.forecastTemperature.text = nil
    }
    
    func configure(data: PreparedWeatherData?) {
        self.forecastImage.image = data?.weatherImage
        self.forecastCity.text = data?.cityName
        self.forecastDate.text = data?.timestamp
        self.forecastTemperature.text = data?.celsiusDegrees
    }
}
