//
//  ForecastWeather.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 28/09/2020.
//

import Foundation

class ForecastWeather: Decodable {
    let list: [WeatherList]
    let city: City
    
    enum CodingKeys: String, CodingKey {
        case list
        case city
    }
}

class WeatherList: Decodable {
    let weather: [Weather]
    let temperature: Tempreture
    let dateString: String
    
    enum CodingKeys: String, CodingKey {
        case weather
        case temperature = "main"
        case dateString = "dt_txt"
    }
}

class City: Decodable {
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case name
    }
}
