//
//  SearchedLocation.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 30/09/2020.
//

import Foundation
import RealmSwift

@objcMembers
class SearchedLocation: Object {
    
    dynamic var locationName: String = ""
    dynamic var timestamp: String = ""
    
    convenience init(
        locationName: String,
        timestamp: String
    ) {
        self.init()
        self.locationName = locationName
        self.timestamp = timestamp
    }
}
