//
//  CurrentWeather.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 25/09/2020.
//

import Foundation

class CurrentWeather: Decodable {
    let coordinates: Coordinates
    let weather: [Weather]
    let temperature: Tempreture
    let cityName: String
    
    enum CodingKeys: String, CodingKey {
        case coordinates = "coord"
        case weather
        case temperature = "main"
        case cityName = "name"
    }
}

class Weather: Decodable {
    let id: Int
    let weatherType: String
    let weatherDescription: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case weatherType = "main"
        case weatherDescription = "description"
    }
}

class Tempreture: Decodable {
    let kelvin: Double
    
    enum CodingKeys: String, CodingKey {
        case kelvin = "temp"
    }
}

class Coordinates: Decodable {
    let longitude: Double
    let latitude: Double
    
    enum CodingKeys: String, CodingKey {
        case longitude = "lon"
        case latitude = "lat"
    }
}
