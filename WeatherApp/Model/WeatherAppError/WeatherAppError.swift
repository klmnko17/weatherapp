//
//  WeatherAppError.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 01/10/2020.
//

import Foundation
import Alamofire

enum WeatherAppError {
    case genericError
    case invalidURL
    case noInternetConnection
    case unauthorized
    case notFound
    case badRequest
    case tooManyRequests
    case impossibleToFetchAddress
    case realmSaveError
    case realmCreateInstanceError
    case realmDeleteError
    case realmDataWasNotPrepared
    
    static func getResponseError(statusCode: Int?, error: AFError) -> WeatherAppError? {
        if let urlError = error.underlyingError as? URLError {
            switch urlError.code {
            case .notConnectedToInternet:
                return WeatherAppError.noInternetConnection
            default:
                break
            }
        }
        guard let statusCode = statusCode else { return nil }
        switch statusCode {
        case 400:
            return WeatherAppError.badRequest
        case 401:
            return WeatherAppError.unauthorized
        case 404:
            return WeatherAppError.notFound
        case 429:
            return WeatherAppError.tooManyRequests
        default:
            return WeatherAppError.genericError
        }
    }
    
    func getErrorString() -> String {
        switch self {
        case .genericError:
            return Strings.genericError
        case .invalidURL:
            return Strings.invalidURL
        case .noInternetConnection:
            return Strings.noInternetConnection
        case .unauthorized:
            return Strings.unauthorized
        case .notFound:
            return Strings.notFound
        case .badRequest:
            return Strings.badRequest
        case .tooManyRequests:
            return Strings.tooManyRequests
        case .impossibleToFetchAddress:
            return Strings.impossibleToFetchAddress
        case .realmSaveError:
            return Strings.realmSaveError
        case .realmCreateInstanceError:
            return Strings.realmCreateInstanceError
        case .realmDeleteError:
            return Strings.realmDeleteError
        case .realmDataWasNotPrepared:
            return Strings.realmDataWasNotPrepared
        }
    }
}
