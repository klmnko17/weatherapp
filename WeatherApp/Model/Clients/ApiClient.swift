//
//  ApiClient.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 25/09/2020.
//

import Foundation
import Alamofire

enum UrlPathType {
    case currentWeatherURL(city: String)
    case forecastWeatherURL(city: String)
    case currentWeatherWithCoordinatesURL
}

class ApiClient {
    
    static let shared = ApiClient()
    private let baseURL = "https://api.openweathermap.org/data/2.5/"
    private let currentWeather = "weather?"
    private let forecastWeather = "forecast?"
    private let locationParameter = "q="
    private let latitudeParameter = "lat"
    private let longitudeParameter = "lon"
    private let openWeatherAppIdParameter = "appid"
    private let openWeatherAPIKey = "48360035303b1b97e59030a67ca82dbe"
    private init() {}

    private func composeURL(urlType: UrlPathType) -> URL? {
        switch urlType {
        case .currentWeatherURL(let city):
            return URL(string: self.baseURL + self.currentWeather + self.locationParameter + city)
        case .forecastWeatherURL(let city):
            return URL(string: self.baseURL + self.forecastWeather + self.locationParameter + city)
        case .currentWeatherWithCoordinatesURL:
            return URL(string: self.baseURL + self.currentWeather)
        }
    }
    
    func fetchCurrentWeather(
        for city: String,
        completionHandler: @escaping (CurrentWeather?, WeatherAppError?) -> ()
    ) {
        guard let url = self.composeURL(
            urlType: .currentWeatherURL(city: city)
        ) else {
            completionHandler(nil, WeatherAppError.invalidURL)
            return
        }
        let params = [
            self.openWeatherAppIdParameter: self.openWeatherAPIKey
        ]
        AF.request(
            url,
            method: .get,
            parameters: params
        )
        .validate()
        .responseDecodable(of: CurrentWeather.self) { (response) in
            switch response.result {
                case .success:
                    completionHandler(response.value, nil)
                case .failure(let error):
                    completionHandler(
                        nil,
                        WeatherAppError.getResponseError(
                            statusCode: response.response?.statusCode,
                            error: error
                        )
                    )
            }
        }
    }
    
    func fetchCurrentWeatherWithCoordinates(
        longitude: String,
        latitude: String,
        completionHandler: @escaping (CurrentWeather?, WeatherAppError?) -> ()
    ) {
        guard let url = self.composeURL(
            urlType: .currentWeatherWithCoordinatesURL
        ) else {
            completionHandler(nil, WeatherAppError.invalidURL)
            return
        }
        let params = [
            self.latitudeParameter: latitude,
            self.longitudeParameter: longitude,
            self.openWeatherAppIdParameter: self.openWeatherAPIKey
        ]
        AF.request(
            url,
            method: .get,
            parameters: params
        )
        .validate()
        .responseDecodable(of: CurrentWeather.self) { (response) in
            switch response.result {
                case .success:
                    completionHandler(response.value, nil)
                case .failure(let error):
                    completionHandler(
                        nil,
                        WeatherAppError.getResponseError(
                            statusCode: response.response?.statusCode,
                            error: error
                        )
                    )
            }
        }
    }
    
    func fetchWeatherForecast(
        for city: String,
        completionHandler: @escaping (ForecastWeather?, WeatherAppError?) -> ()
    ) {
        guard let url = self.composeURL(
            urlType: .forecastWeatherURL(city: city)
        ) else {
            completionHandler(nil, WeatherAppError.invalidURL)
            return
        }
        let params = [
            self.openWeatherAppIdParameter: self.openWeatherAPIKey
        ]
        AF.request(
            url,
            method: .get,
            parameters: params
        )
        .validate()
        .responseDecodable(of: ForecastWeather.self) { (response) in
            switch response.result {
                case .success:
                    completionHandler(response.value, nil)
                case .failure(let error):
                    completionHandler(
                        nil,
                        WeatherAppError.getResponseError(
                            statusCode: response.response?.statusCode,
                            error: error
                        )
                    )
            }
        }
    }
}
