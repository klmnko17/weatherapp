//
//  LocationClient.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 25/09/2020.
//

import Foundation
import CoreLocation

class LocationClient {
    
    static let shared = LocationClient()
    private(set) var locationManager = CLLocationManager()
    var currentLocationCoordinate: CLLocationCoordinate2D? {
        self.locationManager.location?.coordinate
    }
    private init() {}
    
    func getLocationAddress(
        for latitude: CLLocationDegrees,
        for longitude: CLLocationDegrees,
        completion: @escaping (String?) -> ()
    ) {
        let geoCoder = CLGeocoder()
        let location = CLLocation(
            latitude: latitude,
            longitude: longitude
        )
        geoCoder.reverseGeocodeLocation(location) { (placemarks, _) in
            guard let placemark = placemarks?.first else {
                completion(nil)
                return
            }
            let cityName = placemark.locality ?? ""
            completion(cityName)
        }
    }
}
