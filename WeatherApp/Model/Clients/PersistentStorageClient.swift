//
//  PersistentStorageClient.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 29/09/2020.
//

import Foundation
import RealmSwift

class PersistentStorageClient {
    
    static let shared = PersistentStorageClient()
    private var realm: Realm? {
        do {
            return try Realm()
        } catch {
            assertionFailure(WeatherAppError.realmCreateInstanceError.getErrorString())
        }
        return nil
    }
    private let sortKeyPath = "timestamp"
    private init() {}
    
    private func createObject(with cityName: String) -> SearchedLocation {
        SearchedLocation(
            locationName: cityName,
            timestamp: TimestampGenerator.generateCurrentTimeStamp()
        )
    }
    
    private func prepareStorageForNewObject() {
        guard let sortedObjects = loadSortedObjects(),
              let lastSortedObject = sortedObjects.last
        else {
            return
        }
        if sortedObjects.count == 20 {
            do {
                try self.realm?.write {
                    self.realm?.delete(lastSortedObject)
                }
            } catch {
                assertionFailure(WeatherAppError.realmDeleteError.getErrorString())
            }
        }
    }
    
    func saveObject(with cityName: String) {
        let object = createObject(with: cityName)
        self.prepareStorageForNewObject()
        do {
            try self.realm?.write {
                self.realm?.add(object)
            }
        } catch {
            assertionFailure(WeatherAppError.realmSaveError.getErrorString())
        }
    }
    
    func loadSortedObjects() -> Results<SearchedLocation>?  {
        self.realm?.objects(
            SearchedLocation.self
        ).sorted(
            byKeyPath: self.sortKeyPath,
            ascending: false
        )
    }
}
