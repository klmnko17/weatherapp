//
//  SearchHistoryViewController.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 29/09/2020.
//

import UIKit

class SearchHistoryViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    private var viewModel: SearchHistoryViewModel!
    lazy private var searchBar = UISearchBar(frame: .zero)
    var rootViewController: RootViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.createAndConfigureSearchBar()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.viewModel = SearchHistoryViewModel(delegate: self)
        self.viewModel.loadAllSearchedLocations()
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    private func createAndConfigureSearchBar() {
        self.navigationItem.titleView = self.searchBar
        self.searchBar.delegate = self
        self.searchBar.showsCancelButton = true
        self.searchBar.placeholder = Strings.searchBarPlaceholderText
        self.searchBar.becomeFirstResponder()
        self.searchBar.tintColor = .black
    }
}

extension SearchHistoryViewController: SearchHistoryViewModelDelegate {
    func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func refreshData() {
        self.tableView.reloadData()
    }
    
    func passDataToRootViewController(cityName: String) {
        self.rootViewController?.updateLocationCityName(
            cityName: cityName
        )
    }
}

extension SearchHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.getNumberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = Bundle.main.loadNibNamed(
            self.viewModel.cellIdentifier,
            owner: self,
            options: nil
        )?.first as? SearchHistoryCell else { return UITableViewCell() }
        cell.configure(
            data: self.viewModel.prepareCellData(
                indexPath: indexPath
            )
        )
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.viewModel.chooseLocationForWeatherForecast(at: indexPath)
    }
}

extension SearchHistoryViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismissView()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.filterSearchedLocations(
            searchText: searchText.lowercased()
        )
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        EnableSearchBarButton.enableCancelButton(searchBar: searchBar)
        return true
    }
}
