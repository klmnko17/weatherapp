//
//  LocationForecastViewController.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 28/09/2020.
//

import UIKit

class LocationForecastViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    private var viewModel: LocationForecastViewModel!
    var cityName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.viewModel = LocationForecastViewModel(delegate: self)
        self.navigationItem.title = self.viewModel.navigationBarTitle
        self.viewModel.cityName = self.cityName
        self.viewModel.getSearchedLocationCityName()
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.hideLoadingSpinner()
    }
    
    func pushRootScreen(identifier: String) {
        if let rootViewController = UIStoryboard(
            name: identifier,
            bundle: nil
        ).instantiateViewController(
            identifier: identifier
        ) as? RootViewController {
            self.navigationController?.pushViewController(
                rootViewController,
                animated: true
            )
            self.navigationController?.isNavigationBarHidden = true
        }
    }
}

extension LocationForecastViewController: LocationForecastViewModelDelegate {
    func refreshData() {
        self.tableView.reloadData()
    }
}

extension LocationForecastViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.getNumberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = Bundle.main.loadNibNamed(
            self.viewModel.cellIdentifier,
            owner: self,
            options: nil
        )?.first as? LocationForecastCell else { return UITableViewCell() }
        cell.configure(
            data: self.viewModel.prepareCellData(indexPath: indexPath)
        )
        return cell
    }
}
