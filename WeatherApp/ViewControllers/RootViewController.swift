//
//  RootViewController.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 24/09/2020.
//

import UIKit
import MapKit

class RootViewController: BaseViewController {
    
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var weather: UILabel!
    @IBOutlet weak var celsiusDegrees: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var locationSnapshot: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    private var viewModel: RootViewModel!
    lazy private var longTapGesture = UILongPressGestureRecognizer(
        target: self,
        action: #selector(self.longPress)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.addGestureRecognizer(self.longTapGesture)
        self.viewModel = RootViewModel(
            delegate: self,
            locationManagerDelegate: self
        )
        self.viewModel.checkIfLocationServicesAreEnabled()
        self.viewModel.getWeatherDataForCurrentLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func clickOnForecastButton(_ sender: Any) {
        self.pushLocationForecastScreen()
    }
    
    @IBAction func clickOnHistoryButton(_ sender: Any) {
        self.presentSearchHistoryScreen()
    }
    
    @IBAction func clickOnSearchButton(_ sender: Any) {
        self.presentLocationSearchScreen()
    }
    
    @IBAction func clickOnCurrentLocationButton(_ sender: Any) {
        self.viewModel.getWeatherDataForCurrentLocation()
    }
    
    func updateLocationCityName(cityName: String) {
        self.viewModel.getWeatherData(for: cityName)
    }
    
    private func presentLocationSearchScreen() {
        if let locationSearchViewController = UIStoryboard(
            name: self.viewModel.searchScreenIdentifier,
            bundle: nil
        ).instantiateViewController(
            identifier: self.viewModel.searchScreenIdentifier
        ) as? LocationSearchViewController {
            let navController = UINavigationController(
                rootViewController: locationSearchViewController
            )
            locationSearchViewController.rootViewController = self
            self.navigationController?.present(
                navController,
                animated: true,
                completion: nil
            )
        }
    }
    
    private func pushLocationForecastScreen() {
        if let locationForecastViewController = UIStoryboard(
            name: self.viewModel.forecastScreenIdentifier,
            bundle: nil
        ).instantiateViewController(
            identifier: self.viewModel.forecastScreenIdentifier
        ) as? LocationForecastViewController {
            locationForecastViewController.cityName = self.viewModel.cityName
            self.navigationController?.pushViewController(
                locationForecastViewController,
                animated: true
            )
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    private func presentSearchHistoryScreen() {
        if let searchHistoryViewController = UIStoryboard(
            name: self.viewModel.historyScreenIdentifier,
            bundle: nil
        ).instantiateViewController(
            identifier: self.viewModel.historyScreenIdentifier
        ) as? SearchHistoryViewController {
            let navController = UINavigationController(
                rootViewController: searchHistoryViewController
            )
            searchHistoryViewController.rootViewController = self
            self.navigationController?.present(
                navController,
                animated: true,
                completion: nil
            )
        }
    }
    
    @objc
    private func longPress(sender: UIGestureRecognizer) {
        if sender.state == .began {
            let locationInView = sender.location(in: self.mapView)
            let locationOnMap = self.mapView.convert(
                locationInView,
                toCoordinateFrom: self.mapView
            )
            self.viewModel.saveMapLocationCoordinates(coordinates: locationOnMap)
        }
    }
    
    private func animateWeatherCardDataChanging() {
        self.weather.fadeTransition(0.5)
        self.cityName.fadeTransition(0.5)
        self.celsiusDegrees.fadeTransition(0.5)
        self.weatherImage.fadeTransition(0.5)
        self.locationSnapshot.fadeTransition(0.5)
    }
    
    func updateWeatherCard(preparedData: PreparedWeatherData) {
        self.cityName.text = preparedData.cityName
        self.weather.text = preparedData.weatherDescription
        self.celsiusDegrees.text = preparedData.celsiusDegrees
        self.weatherImage.image = preparedData.weatherImage
        self.locationSnapshot.image = preparedData.snapshotImage
        self.animateWeatherCardDataChanging()
        self.stopAnimatingActivityIndicator()
    }
    
    func startAnimatingActivityIndicator() {
        self.activityIndicator.startAnimating()
    }
    
    func stopAnimatingActivityIndicator() {
        self.activityIndicator.stopAnimating()
    }
}

extension RootViewController: RootViewModelDelegate {
    func setMapViewRegion(region: MKCoordinateRegion) {
        self.mapView.setRegion(
            region,
            animated: true
        )
    }
    
    func preparePointForMapAnnotation(
        region: MKCoordinateRegion,
        snapshot: MKMapSnapshotter.Snapshot,
        snapshotImage: UIImage,
        finalImageRect: CGRect,
        mapAnnotation: MKPinAnnotationView,
        mapAnnotationImage: UIImage
    ) {
        self.mapView.annotations.forEach { _ in
            var snapshotPoint = snapshot.point(for: region.center)
            if finalImageRect.contains(snapshotPoint) {
                let mapAnnotationCenterOffset = mapAnnotation.centerOffset
                snapshotPoint.x -= mapAnnotation.bounds.size.width / 2.0
                snapshotPoint.y -= mapAnnotation.bounds.size.height / 2.0
                snapshotPoint.x += mapAnnotationCenterOffset.x
                snapshotPoint.y += mapAnnotationCenterOffset.y
                mapAnnotationImage.draw(at: snapshotPoint)
            }
        }
    }
}

extension RootViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.viewModel.checkLocationAuthorizationStatus()
    }
}
