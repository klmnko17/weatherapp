//
//  LocationSearchViewController.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 26/09/2020.
//

import UIKit
import MapKit

class LocationSearchViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var viewModel: LocationSearchViewModel!
    private var localSearchCompleter: MKLocalSearchCompleter!
    lazy private var searchBar = UISearchBar(frame: .zero)
    var rootViewController: RootViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.createAndConfigureSearchBar()
        self.viewModel = LocationSearchViewModel(delegate: self)
        self.createAndConfigureSearchCompleter()
        self.tableView.tableFooterView = UIView()
    }
    
    private func createAndConfigureSearchBar() {
        self.navigationItem.titleView = self.searchBar
        self.searchBar.delegate = self
        self.searchBar.showsCancelButton = true
        self.searchBar.placeholder = Strings.searchBarPlaceholderText
        self.searchBar.becomeFirstResponder()
        self.searchBar.tintColor = .black
    }
    
    private func createAndConfigureSearchCompleter() {
        self.localSearchCompleter = MKLocalSearchCompleter()
        self.localSearchCompleter.delegate = self
        self.localSearchCompleter.resultTypes = .address
    }
}

extension LocationSearchViewController: LocationSearchViewModelDelegate {
    func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func refreshData() {
        self.tableView.reloadData()
    }
    
    func passDataToRootViewController(cityName: String) {
        self.rootViewController?.updateLocationCityName(
            cityName: cityName
        )
    }
}

extension LocationSearchViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismissView()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.processTextChangeInSearchBar(
            searchText: searchText,
            localSearchCompleter: self.localSearchCompleter
        )
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        EnableSearchBarButton.enableCancelButton(searchBar: searchBar)
        return true
    }
}

extension LocationSearchViewController: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        self.viewModel.searchResults = completer.results.map { $0.title }
    }
}

extension LocationSearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = Bundle.main.loadNibNamed(
            self.viewModel.cellIdentifier,
            owner: self,
            options: nil
        )?.first as? SearchResultCell else { return UITableViewCell() }
        cell.configureCell(
            locationName: self.viewModel.searchResults[indexPath.row]
        )
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.viewModel.chooseLocationForWeatherForecast(at: indexPath)
    }
}
