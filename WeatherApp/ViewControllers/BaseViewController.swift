//
//  BaseViewController.swift
//  WeatherApp
//
//  Created by Valerii Klymenko on 25/09/2020.
//

import UIKit
import SVProgressHUD

class BaseViewController: UIViewController {
    
    func showInfoAlert(
        title: String,
        message: String,
        completion: (() -> Void)?
    ) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        let cancelAction = UIAlertAction(
            title: Strings.ok,
            style: .cancel,
            handler: { _ in
                alert.dismiss(animated: true) {
                    completion?()
                }
            }
        )
        alert.addAction(cancelAction)
        self.present(
            alert,
            animated: true,
            completion: nil
        )
    }
    
    private func configureAlertWithActions(
        alertTitle: String,
        alertMessage: String,
        cancelActionTitle: String,
        defaultActionTitle: String,
        preferredActionIsDefault: Bool,
        completion: @escaping () -> ()
    ) {
        let alert = UIAlertController(
            title: alertTitle,
            message: alertMessage,
            preferredStyle: .alert
        )
        let cancelAction = UIAlertAction(
            title: cancelActionTitle,
            style: .cancel,
            handler: { _ in
                alert.dismiss(animated: true, completion: nil)
            }
        )
        let defaultAction = UIAlertAction(
            title: defaultActionTitle,
            style: .default,
            handler: { _ in
                completion()
            }
        )
        alert.addAction(cancelAction)
        alert.addAction(defaultAction)
        if preferredActionIsDefault {
            alert.preferredAction = defaultAction
        } else {
            alert.preferredAction = cancelAction
        }
        self.present(
            alert,
            animated: true,
            completion: nil
        )
    }
    
    func showChangeWeatherForLocationAlert(
        title: String,
        message: String,
        completion: @escaping () -> ()
    ) {
        self.configureAlertWithActions(
            alertTitle: title,
            alertMessage: message,
            cancelActionTitle: Strings.cancel,
            defaultActionTitle: Strings.yes,
            preferredActionIsDefault: false
        ) {
            completion()
        }
    }
}

extension BaseViewController {
    
    // MARK: Spinner Methods
    func showLoadingSpinner() {
        SVProgressHUD.setBackgroundColor(.clear)
        SVProgressHUD.show()
    }

    func hideLoadingSpinner() {
        SVProgressHUD.dismiss()
    }
}
